# this file is used by target

@liaisons = {
	'alpha' => ['armin76',		'klausman'],
	'amd64' => ['keytoaster',	'chainsaw'],
	'hppa'  => ['jer'],
	'ppc'   => ['josejx',		'ranger'],
	'ppc64' => ['josejx',		'ranger'],
	'sparc' => ['armin76'],
	'x86'   => ['maekke'],
	'release'=> ['pva']
}
